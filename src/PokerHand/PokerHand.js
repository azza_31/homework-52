import React from "react";

class PokerHand {
    getCardFlash = array => {
        let count = 0;
        for (let i = 0; i < array.length; i++) {
            const num = i + 1;
            for (let j = num; j < array.length; j++) {
                if (array[i].suit !== array[j].suit) {
                    count++;
                }
            }
        }

        return count;
    };

    getCardPare = array => {
        let count = 0;
        for (let i = 0; i < array.length; i++) {
            for (let j = 0; j < array.length; j++) {
                if (array[i].rank === array[j].rank && i !== j) {
                    count++;
                }
            }
        }

        return count;
    };

    getCardCombination(array) {
        if (this.getCardFlash(array) === 5) {
            return 'Flash';
        } else if (this.getCardPare(array) === 1) {
            return 'One pair';
        } else if (this.getCardPare(array) === 2) {
            return 'Two pairs';
        } else if (this.getCardPare(array) === 3) {
            return 'Three of a kind';
        } else if (this.getCardPare(array) === 4) {
            return 'Four of a kind';
        } else if (this.getCardPare(array) === 0) {
            return 'Try again';
        } else {
            return "Error"
        }
    };
}

export default PokerHand;