import React from "react";

class CardDeck {
    static SUITS = {
        HEARTS: 'HEARTS',
        DIAMONDS: 'DIAMONDS',
        CLUBS: 'CLUBS',
        SPADES: 'SPADES'
    };

    static RANKS = {
        TWO: '2',
        THREE: '3',
        FOUR: '4',
        FIVE: '5',
        SIX: '6',
        SEVEN: '7',
        EIGHT: '8',
        NINE: '9',
        TEN: '10',
        Jack: 'J',
        QUEEN: 'Q',
        KING: 'K',
        ACE: 'A'
    };

    cards = [];

    constructor() {
        for (let suit in CardDeck.SUITS) {
            for (let rank in CardDeck.RANKS) {
                this.cards.push({
                    suit: CardDeck.SUITS[suit],
                    rank: CardDeck.RANKS[rank]
                });
            }
        }
    }

    getCard() {
        let randCard = Math.floor(Math.random() * this.cards.length);
        let card = this.cards.splice(randCard, 1);
        console.log(card);
        return card[0];
    }

    getCards(howMany) {
        const cards = [];

        for (let i = 0; i < howMany; i++) {
            cards.push(this.getCard());
        }

        return cards;
    }
}

export default CardDeck;