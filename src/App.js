import React from "react";
import './App.css';
import CardDeck from "./CardDeck/CardDeck";
import Card from "./Card/Card";
import './Card.css';
import PokerHand from "./PokerHand/PokerHand";

class App extends React.Component {
    state = {
        cards: [],
        combo: 'Try playing !'
    };

    getNewCardDeck = () => {
        const cardDeck = new CardDeck();
        const cards = cardDeck.getCards(5);
        const combo = this.getOutCome(cards);

        console.log("hand", cards);

        this.setState({
            cards: cards,
            combo
        });
    };

    getOutCome = (cards) => {
        const pokerHand = new PokerHand();
        return pokerHand.getCardCombination(cards);
    };

    render() {

        return (
            <div className="App">
                <div className="playingCards">
                    {this.state.cards && this.state.cards.map((card, index) => {
                        return (
                            <Card key={index} suit={card.suit} rank={card.rank}/>
                        )
                    })}
                </div>
                <div>
                    <b className="text-combo">{this.state.combo}</b>
                </div>
                <div>
                    <button className="btn" onClick={this.getNewCardDeck}>New Cards</button>
                </div>
            </div>
        );
    }
}

export default App;
