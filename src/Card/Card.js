import React from "react";

const suits = {
    HEARTS: {className: 'Card-hearts', symbol: '♥'},
    DIAMONDS: {className: 'Card-diam', symbol: '♦'},
    CLUBS: {className: 'Card-clubs', symbol: '♣'},
    SPADES: {className: 'Card-spades', symbol: '♠'}
};

const Card = props => {
    const suitClass = suits[props.suit].className;
    const symbol = suits[props.suit].symbol;

    const cardClasses = [
        'Card',
        'Card-link-' + props.rank.toLowerCase(),
        suitClass,
    ];

    if (props.back) {
        cardClasses.push('Card-back');
    }

    return (
        <div className={cardClasses.join(' ')}>
            <span className="Card-rank">{props.rank.toUpperCase()}</span>
            <span className="Card-suit">{symbol}</span>
        </div>
    )
};

export default Card;